package com.marcinklisiak.tools.spisim;

import com.marcinklisiak.tools.spisim.ui.MainFrame;

public class StepPISimulator {

	public static void main(String[] args) {
		final StepPIController controller = new StepPIController();

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI(controller);
            }
        });

		new Thread(controller).start();
	}

	private static void createAndShowGUI(StepPIController controller) {
        //Create and set up the window.
        MainFrame frame = new MainFrame(controller);
        controller.addObserver(frame);
        frame.pack();
        frame.setVisible(true);
    }
}
