package com.marcinklisiak.tools.spisim;

import java.util.Observable;

public class StepPIController extends Observable implements Runnable {

	public static final double Tw = 180;
	public static final double Kw = 8;
	public static final double t0 = 1.0;
	public static final double DELTA = 0.1;
	public static final double h = 0.05;
	public static final double T_m = 60;

	private int y1 = 0;
	private double v = 0.0;
	private double E = 0.0;
	private double W = 0.0;

	//input params
	private double setpoint = 20.0;
	private volatile double current = 20.0;

	//output
	private int y1_t0 = 0;
	private double position = 0.0;

	public StepPIController() {
	}

	public int getY1_t0() {
		return y1_t0;
	}

	public double getPosition() {
		return position;
	}

	public void setSetpoint(double setpoint) {
		this.setpoint = setpoint;
	}

	public void setCurrent(double current) {
		this.current = current;
	}

	@Override
	public void run() {
		while (true) {
			v = Math.exp(-t0 / Tw) * (v + 0.5 * t0 * Kw / Tw * (y1 + Math.exp(t0 / Tw) * y1_t0));
			E = setpoint - current;
			W = E - v;
			y1 = y1_t0;

			//1 - OPENING
			if (W > DELTA || (y1 == 1 && W > DELTA - h)) {
				y1_t0 = 1;
			}
			//-1 - CLOSING
			else if (W < -DELTA || (y1 == -1 && W < -DELTA + h)) {
				y1_t0 = -1;
			}
			//0
			else {
				y1_t0 = 0;
			}

			position = updatePosition();
			try {
				Thread.sleep((long)(t0 * 200));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			setChanged();
			notifyObservers(new float[] {(float)current, (float)y1_t0, (float)position});
		}
	}

	private double updatePosition() {
		double newPosition = position + t0 * y1_t0;

		newPosition = Math.max(newPosition, 0.0);
		newPosition = Math.min(newPosition, T_m);

		return newPosition;
	}
}
