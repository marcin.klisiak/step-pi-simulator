package com.marcinklisiak.tools.spisim.ui;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Second;
import org.jfree.ui.ApplicationFrame;

import com.marcinklisiak.tools.spisim.StepPIController;

public class MainFrame extends ApplicationFrame implements Observer {

	private ChartPanel panel;
	private JFreeChart chart;

	private DynamicTimeSeriesCollection dataset;

	public MainFrame(final StepPIController controller) {
        super("Step PI Simulator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.dataset = new DynamicTimeSeriesCollection(3, 3 * 60, new Second());
        this.dataset.setTimeBase(new Second(0, 0, 0, 1, 1, 2011));
        this.dataset.addSeries(new float[]{0}, 0, "Current");
        this.dataset.addSeries(new float[]{0}, 1, "Signal");
        this.dataset.addSeries(new float[]{0}, 2, "Position");

        this.chart = ChartFactory.createTimeSeriesChart(
        		"Simulation results", "hh:mm:ss", "values", this.dataset, true, true, false);

        this.panel = new ChartPanel(chart);
        add(this.panel, BorderLayout.CENTER);

        JLabel l = new JLabel("Current: ");
        add(l, BorderLayout.SOUTH);

        JSpinner spinner = new JSpinner(new SpinnerNumberModel(20.0, 15.0, 25.0, 0.01));
        spinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				JSpinner spinner = (JSpinner) e.getSource();
			    controller.setCurrent(((Number) spinner.getValue()).doubleValue());
			}

        });

        l.setLabelFor(spinner);
        add(spinner, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable o, Object arg) {
		float[] values = (float[]) arg;

		this.dataset.advanceTime();
		this.dataset.appendData(values);
	}
}
